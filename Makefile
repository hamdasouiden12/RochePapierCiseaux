CC=gcc
CFLAGS=-std=c99 -Wall

# additional flags for gcov
TESTFLAGS=-fprofile-arcs -ftest-coverage

testcomparaison: testcomparaison.c jeux.h jeux.c
	# build the test
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testcomparaison testcomparaison.c jeux.c

	# run the test, which will generate testcomparaison.gcda and testcomparaison.gcno
	./testcomparaison


testhasard: testhasard.c jeux.h jeux.c
	# build the test
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testhasard testhasard.c jeux.c

	# run the test, which will generate testhasard.gcda and testhasard.gcno
	./testhasard


clean:
	rm -f *.o testhasard testcomparaison *.gcov *.gcda *.gcno
