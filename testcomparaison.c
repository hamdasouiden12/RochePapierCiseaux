#include <stdio.h>
#include <assert.h>
#include "jeux.h"

int main() {
    assert(comparaison('R' , 'R') == 0);
    assert(comparaison('P' , 'P') == 0);
    assert(comparaison('C' , 'C') == 0);
    assert(comparaison('R' , 'C') == 1);
    assert(comparaison('C' , 'P') == 1);
    assert(comparaison('P' , 'R') == 1);
    assert(comparaison('C' , 'R') == -1);
    assert(comparaison('R' , 'P') == -1);
    assert(comparaison('P' , 'C') == -1);
    
    printf("test unitaire pour comparaison réussi.\n");
    return 0;
}
